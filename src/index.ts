console.log('hello world!!!');


//добавлен новый файл banana.ts
//Добавлен новый файл pineapple.ts



//Для добавления и установки значения применяется метод set():
const myMap = new Map([[1, "a"], [2, "b"], [3, "c"]]);
console.log(myMap);  
myMap.set(4, "d");      // добавление элемента
myMap.set(2, "v");      // изменение элемента
console.log(myMap);

//получения элемента по ключу применяется метод get()
const myMap0 = new Map([[1, "Tom"], [2, "Bob"], [3, "Sam"]]);
console.log(myMap0.get(2));  // Bob
console.log(myMap0.get(7));  // undefined


//удаление delete(), clear()
const myMap1 = new Map([[1, "Tom"], [2, "Bob"], [3, "Sam"]]);
myMap1.delete(2);
console.log(myMap1);     // Map(2) {1 => "Tom", 3 => "Sam"}

const myMap2 = new Map([[1, "Tom"], [2, "Bob"], [3, "Sam"]]);
myMap2.clear();
console.log(myMap2);     // Map(0) {}

// forEach и for of перебор

const myMap3 = new Map([[1, "Tom"], [2, "Bob"], [3, "Sam"]]);
 
myMap3.forEach(function(value1, value2, map){
    console.log(value2, value1);
})
 

// for(item of myMap3){
//     console.log(item[0], item[1]);
// }


// const myMap4 = new Map([[1, "Tom"], [2, "Bob"], [3, "Sam"]]);
 
// for(item of myMap4.keys()){
//     console.log(item);
// }
// for(item of myMap4.values()){
//     console.log(item);
// }
